---
home: true
heroImage: /assets/logo.png
actionText: 快速上手 →
actionLink: /guide/
features:
- title: 分布式云原生架构一站式解决方案
  details: Mendmix提供了数据库、缓存、消息中间件、分布式定时任务、安全框架、网关以及主流产商云服务快速集成能力。基于Mendmix可以不用关注技术细节快速搭建高并发高可用基于微服务的分布式架构。。
- title: 简单易用
  details: Mendmix全部基于主流框架只做增强不做任何底层定制修改，最小依赖运行、配置驱动兼容各种部署场景。
- title: 稳定可靠
  details: Mendmix持续7年不断迭代、完成了三家大型企业从0到1微服务架构升级、技术中台建设以及企业上云。

footerColumn: 3
footerWrap: 
- headline: 源码
  items:
  - title: github
    link: https://github.com/vakinge
  - title: 码云
    link: https://gitee.com/vakinge
- headline: 社区交流
  items:
  - title: 学习交流
    link: http://www.mendmix.com
    details: QQ群/微信群	
  - title: bug反馈
    link: http://www.mendmix.com
    details: Github issue
- headline: 联系作者
  items:
  - title: 作者微信
    link: http://www.mendmix.com
    details: x3b4f07082
  - title: 作者博客
    link: https://blog.csdn.net/vakinge
footer: Apache-2.0 | Copyright © 2020 Jeesuite.com | <a href="http://beian.miit.gov.cn/" target="_blank">粤ICP备17101749号-1</a>
---
### 我的技术站点
 - [vakinge@码云](https://gitee.com/vakinge)
 - [vakinge@Github](https://github.com/vakinge)
 - [vakinge@CSDN](https://blog.csdn.net/vakinge)
 
### 微信交流群(群二维码过期，请先加作者微信)
<img src="https://jeesuite.oss-cn-guangzhou.aliyuncs.com/weixin_qrcode.jpeg" width="180" height="180" />

