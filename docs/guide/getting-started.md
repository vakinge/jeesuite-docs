---
title: 快速开始
---

# 说明
为了帮助大家快速开始学习Jeesuite，我们提供了一套基于`Spring Cloud`整合`Jeesuite`的示例项目。接下里将讲解如何快速本地运行项目。

# 步骤
## 搭建配置中心
请参考文档[配置中心](../docments/config-center.html)在本地运行配置中心，需要导入可选的测试数据。

## 下载项目示例项目
```
git clone https://gitee.com/vakinge/jeesuite-demo.git
```

## 启动注册中心
```
cd ./jeesuite-demo/demo-eureka-server
./application.sh start
```
启动完成，打开：`http://127.0.0.1:8761/`，账号密码：admin/admin123

## 启动网关
```
cd ./jeesuite-demo/demo-apigataway
./application.sh start
```

启动完成，打开：`http://127.0.0.1:8080/`，账号密码：admin/admin123,此时可以登录，但是`数据管理`还不能正常访问。

## 启动模板服务

由于该服务需要用到数据库，所以启动前需要完成两件准备工作。
1. 支持`docs/init-table.sql`初始化数据库表
2. 登录配置中心→配置管理，找到`模板项目`编辑配置的数据库配置即可。
```
cd ./jeesuite-demo/demo-template-project
./application.sh start
```
启动完成后，刷新eureka注册中心，看看注册情况，注册成功，再打开`数据管理`即可。

**说明**： 以上只适合linux或者mac，如果是window，最简单的方式是直接导入IDE，在IDE分别启动。

## 生成DAO层

打开 `/src/test/resources/generatorConfig.xml`，编辑数据库连接信息，内容如下：

```xml
<jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
	connectionURL="jdbc:mysql://127.0.0.1:3306/demo_db?serverTimezone=UTC&amp;characterEncoding=utf-8&amp;nullCatalogMeansCurrent=true" userId="root">
</jdbcConnection>
```

替换我们要生成的数据库表，配置如下：

```xml
<table tableName="sys_account" domainObjectName="AccountEntity">
  <generatedKey column="id" sqlStatement="Mysql" identity="true" />
</table>	
```

配置说明：

 - jdbcConnection：连接数据库配置，如果用密码的话，加上 `passport` 属性即可。
 - targetProject：输出的目录路径
 - targetPackage：输出的包路径
 - table：要生成的表，如果不是自增ID，就不需要配置 `generatedKey` 项。
 
配置完成，我们执行单元测试方法生成 Entity、Mapper 文件。

```bash
mvn clean test -Dtest=helper.MyBatisGeneratorTest#testRun
```

## 基于模板项目生成其他微服务
在`demo-template-project`目录下执行以下脚本生成新的微服务
```bash
# 参数：项目前缀，base包名，模块名称
./projectgen.sh demo com.jeesuite.demo account
```


## 开启redis缓存
模板服务提供了一个缓存测试接口：`http://127.0.0.1:8080/api/template/demo/cacheTest`。要开启缓存只需要在配置中心增加如下配置：
```
jeesuite.cache.servers=127.0.0.1:6379
jeesuite.cache.password=123456
jeesuite.cache.database=0
```
然后重启服务即可。

## 定任务支持分布式
>模板项目启动了一个demoTask定时任务，如果需要支持分布式，只需要增加一个配置指定注册zookeeper地址即可。

编辑配置中心配置，增加以下配置：
```
jeesuite.task.registryType=zookeeper
jeesuite.task.registryServers=127.0.0.1:2181
```


