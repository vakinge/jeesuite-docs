---
title: 更新日志
---

## 1.3.8
jeesuite-common
```
ADD httpUtils 底层适配jdk原生、okhttp3、apache httpclient
ADD GUID工具类，支持雪花算法及带时间序列的全局唯一ID生成
ADD base58工具类
```

jeesuite-security
```
本模块底层做了很大的重构，代码更加清晰
```
其他
```
WebUtils增加一些方法
升级一些依赖框架版本

```

## 1.3.7
jeesuite-mybatis
```
ADD 数据变更记录采集处理器
ADD 数据权限支持按表名配置
UPDATE page类移至jeesuite-common模块
UPDATE 自动缓存配置简化
UPDATE mybatis 版本升级
```
jeesuite-scheduler
```
UPDATE 重写持久化接口定义
ADD 提供对外统一状态查询接口
```

## 1.3.6
jeesuite-springweb
```
ADD 集成jeesuite-security
ADD 分布式web服务全局拦截器
ADD 全局上下文机制
ADD springcloud一些基础类支持
```
jeesuite-mybatis
```
FIXED mapper多层继承解析方法问题
FIXED mybatis插件执行顺序逻辑调整
```

jeesuite-common
```
ADD 全局加密类
```

## 1.3.5
jeesuite-mybatis
```
ADD 支持多租户
ADD 数据权限插件
```
jeesuite-cache
```
ADD 支持多租户
```
jeesuite-scheduler
```
ADD 支持多租户
ADD 增加定时任务禁用配置
```
jeesuite-common
```
ADD 全局管理框架所有线程变量
```

## 1.3.4
jeesuite-mybatis
```
重写自动缓存插件
```
jeesuite-filesystem
```
增加腾讯云COS支持
```
jeesuite-common
```
本地配置解析支持profile
```
jeesuite-cache
```
ADD sortset方法
```

## 1.3.3 
jeesuite-security
```
ADD Oauth2.0支持
```

jeesuite-filesystem
```
ADD 七牛SDK升级相关代码修改
```

jeesuite-common
```
ADD 增加一些工具类
```
 
## 1.3.2 
jeesuite-mybatis
```
ADD 分页DTO转换接口
```
 
jeesuite-kafka
```
ADD 动态消息支持
```

jeesuite-cache
```
ADD redis支持无密码配置
```

jeesuite-common2
```
FIXED redis分布式锁实现优化
```
 
## 1.3.1
jeesuite-security
```
ADD 可选本地session和共享session
ADD 可选是否支持多端同时登录
ADD dubbo、springboot跨服务登录状态传递支持
```
 
jeesuite-mybaits
```
FIXED 重构mybaits增强插件注册逻辑简化配置
ADD 开放自定义mybatis插件hander接口
ADD 增加敏感操作拦截mybatis插件hander
ADD 支持无缝集成CRUD增强框架mapper
UPDATE 升级mybatis版本去掉自动CRUD过期代码
```
jeesuite-kafka
```
FIXED 修复json消息反序列化漏处理header字段
```
 
jeesuite-scheduler
```
ADD 新增单机模式任务监控支持
```

jeesuite-springboot-starter
```
FIXED 重构mybaits模块注册逻辑
FIXED scheduler模块注册兼容springboot2.x
```
 
jeesuite-springweb
```
FIXED 日志拦截器请求内容过长截取输出
```
