---
title: 安全认证模块
---

# 功能说明
 * 配置简单(初始化一个类即可)
 * 满足认证授权基本需求
 * 更加贴近日常使用业务场景
 * 可选本地session和基于redis存储共享session
 * 可选是否支持多端同时登录
 * dubbo、springboot跨服务登录状态传递支持

# 使用说明

## 添加依赖

```
<dependency>
    <groupId>com.mendmix</groupId>
    <artifactId>mendmix-security</artifactId>
    <version>[最新版本]</version>
</dependency>
```

#### 如何集成
没有复杂配置，实现抽象类`SecurityConfigurerProvider`即可，例如：
```java
@Component
public class DemoSecurityConfigurerProvider extends SecurityConfigurerProvider<AuthUser> {
	
	private static List<String> anonymousUrlPatterns = Arrays.asList("/user/login");
	
	@Override
	public String contextPath() {
		return ResourceUtils.getProperty("server.servlet.context-path", "");
	}

	/**
	 * 可匿名访问接口，可使用“*”通配符
	 */
	@Override
	public List<String> anonymousUrlPatterns() {
		return anonymousUrlPatterns;
	}

	@Override
	public AuthUser validateUser(String name, String password)
			throws UserNotFoundException, UserPasswordWrongException {
		//TODO 模拟调用用户服务验证用户
		if(!StringUtils.equals(name, "admin")) {
			throw new UserNotFoundException();
		}
		
		if(!StringUtils.equals(password, "admin123")) {
			throw new UserPasswordWrongException();
		}

		AuthUser userInfo = new AuthUser();
		userInfo.setId("1000");
		userInfo.setName(name);

		return userInfo;
	}


	/**
	 * 所有需要授权的接口列表
	 */
	@Override
	public List<String> findAllUriPermissionCodes() {
		//这里应该从权限系统获取，为了演示我们就写死
		return Arrays.asList("/api/user/*");
	}


	/**
	 * 某个用户拥有的接口权限列表
	 */
	@Override
	public List<String> getUserPermissionCodes(String userId) {
		//这里应该从权限系统获取，为了演示我们就写死
		if("10000".equals(userId)) {
			return Arrays.asList("/api/user/*");		
		}
		return null;
	}

	@Override
	public void authorizedPostHandle(UserSession session) {
		//透传登录用户信息
		RequestContext.getCurrentContext().addZuulRequestHeader(WebConstants.HEADER_AUTH_USER,
				session.getUserInfo().toEncodeString());
		//透传当前租户信息
		RequestContext.getCurrentContext().addZuulRequestHeader(WebConstants.HEADER_TENANT_ID,
				session.getTenantId());
	}

	/**
	 * 超管账号
	 */
	@Override
	public String superAdminName() {
		return "sa";
	}

	@Override
	public String _401_Error_Page() {
		return null;
	}

	@Override
	public String _403_Error_Page() {
		return null;
	}
}
```

## 登入登出
```java
@PostMapping("login")
public WrapperResponse<AuthUser> login(@RequestBody Map<String, String> param) {
	//TODO 验证码 、字段校验
	AuthUser userInfo = SecurityDelegating.doAuthentication(param.get("name"), param.get("password")).getUserInfo();
	return new WrapperResponse<AuthUser>(userInfo);
}
	
@PostMapping(value = "logout")
public @ResponseBody WrapperResponse<String> logout(HttpServletRequest request,HttpServletResponse response) {
	SecurityDelegating.doLogout();
	return new WrapperResponse<>();
} 
```