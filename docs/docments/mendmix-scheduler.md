---
title: 定时任务模块
---

# 功能说明

* 支持分布式保证单节点执行（按节点平均分配job）
* 支持failvoer，自动切换故障节点
* 支持多节点下并行计算
* 支持无注册中心单机模式
* 支持自定义重试策略
* 支持配置持久化（启动加载、变更保存）
* 直接监控、停止/运行、执行等操作API

# 使用说明

## 添加依赖

```
<dependency>
    <groupId>com.mendmix</groupId>
    <artifactId>mendmix-scheduler</artifactId>
    <version>[最新版本]</version>
</dependency>
```

## 定时任务开发
```java
@Service
@ScheduleConf(cronExpr="0/30 * * * * ?",jobName="demoTask",executeOnStarted = true)
public class DemoTask extends AbstractJob{

	int count = 1;
	@Override
	public void doJob(JobContext context) throws Exception {
		//do something
	}

	@Override
	public boolean parallelEnabled() {
	    // 分布式下开启并行计算返回true
		return false;
	}

}
```
`ScheduleConf`注解说明：
  - cronExpr：时间表达式
  - jobName：任务名，一个任务组下唯一
  - executeOnStarted：启动时是否执行一次

## 定时任务注册

### spring XML方式
```xml
 <!-- 注册中心（集群管理） -->
    <bean id="jobRegistry" class="com.mendmix.scheduler.registry.ZkJobRegistry">
       <property name="zkServers" value="localhost:2181" />
    </bean> 
   
	<bean class="com.mendmix.scheduler.SchedulerFactoryBeanWrapper" >
	    <!-- 确保每个应用groupName唯一 -->
	    <property name="groupName" value="demo" />
	    <property name="registry" ref="jobRegistry" /> 
	    <property name="scanPackages" value="com.mendmix.test.sch" />
		<!--持久化处理器 ,可选-->
		<property name="persistHandler">
			<bean class="com.mendmix.test.sch.DbConfigPersistHandler" />
		</property> 
	</bean>
```
>单机运行(不需要zookeeper)使用注册中心实现类`com.mendmix.scheduler.registry.NullJobRegistry`即可

### springboot方式
#### 添加依赖
```xml
<dependency>
    <groupId>com.mendmix</groupId>
    <artifactId>mendmix-springboot-starter</artifactId>
    <version>[最新版本]</version>
</dependency>
```

#### 启动注解
```
@EnablemendmixSchedule
```

#### 添加配置
```
mendmix.task.registryType=zookeeper
mendmix.task.registryServers=127.0.0.1:2181
mendmix.task.groupName=demo
#扫描@ScheduleConf注解的包
mendmix.task.scanPackages=com.mendmix.demo.task
mendmix.task.threadPoolSize=3
```

>单机运行(不需要zookeeper)设置`mendmix.task.registryType=none`即可

## 多租户支持
定时任务目前支持独立数据源的多租户模式，只需讲task类继承`AbstractJob`改为`AbstractMultTenantJob`即可,如：
```java
@Service
@ScheduleConf(cronExpr="0/30 * * * * ?",jobName="demoTask",executeOnStarted = true)
public class DemoTask extends AbstractMultTenantJob{

	int count = 1;
	@Override
	public void doJob(JobContext context) throws Exception {
		//do something
	}

	@Override
	public boolean parallelEnabled() {
	    // 分布式下开启并行计算返回true
		return false;
	}

}
```

## restAPI
### 只需要注册Servlet即可启用，以springboot为例，启动类添加以下注解即可：
```java
@ServletComponentScan({"com.mendmix.scheduler.api"})
```
### 查询状态
```
curl "http://{baseUrl}/scheduler/status"
```