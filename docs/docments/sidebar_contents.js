module.exports = [
	"/docments/",
	"mendmix-common",
	"mendmix-spring",
	"mendmix-mybatis",
	"mendmix-cache",
	"mendmix-scheduler",
	"mendmix-security",
	"mendmix-springweb",
	"mendmix-amqp-adapter",
	"mendmix-cos-adapter",
	"config-center",
	"auth-center",
];
