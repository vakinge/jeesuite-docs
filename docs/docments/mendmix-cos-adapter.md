---
title: 云存储适配模块
---

# 介绍
目前各大云厂商都提供了云对象存储服务(COS)，为了简化接入成本以及实现一套代码兼容各种云厂商，于是就有了本项目。本项目作为各云存储服务的中间适配层，只需要修改配置即可轻松切换云存储服务提供商。

# 使用说明

## 添加依赖
```xml
<dependency>
	<groupId>com.mendmix</groupId>
	<artifactId>mendmix-cos-adapter</artifactId>
	<version>1.0.0</version>
</dependency>

<!--接入七牛需加入-->
<dependency>
	<groupId>com.qiniu</groupId>
	<artifactId>qiniu-java-sdk</artifactId>
	<version>[7.0.0, 7.2.99]</version>
</dependency>

<!--接入阿里云需加入-->
<dependency>
	<groupId>com.aliyun.oss</groupId>
	<artifactId>aliyun-sdk-oss</artifactId>
	<version>3.11.3</version>
</dependency>

<!--接入腾讯云需加入-->
<dependency>
	<groupId>com.qcloud</groupId>
	<artifactId>cos_api</artifactId>
	<version>5.6.39</version>
</dependency>
```
## 配置说明

配置项 | 必填 | 说明
---|---|---
accessKey  | 是 | 云服务商提供
secretKey  | 是 | 云服务商提供
appId      | 否 | 目前仅腾讯云需要
regionName | 是 | 七牛云(如:huanan)，腾讯云(如:ap-guangzhou),阿里云(如:cn-guangzhou)
defaultBucketName | 否 | 如果上传不指定bucket，就使用默认的
maxFileSize | 否 | 支持最大文件大小，默认不限制
maxConnections | 否 | 客户端最大连接数，默认：100
isPrivate | 否 | 是否私有
urlPrefix | 否 | 自定义绑定域名或转发规则的baseUrl


## 初始化客户端
###  默认方式
如果服务只存在一套文件服务配置，我们提供了一个默认客户端。只需要提供如下配置：
```
#可选：aliyun,qcloud,qiniu
mendmix.cos.adapter.type=qiniu
mendmix.cos.adapter.accessKey=
mendmix.cos.adapter.xxx= （参考上述配置）
```

用法：
```java
CosDefaultClientBuilder.getProvider().upload(object);
```

### Spring XML配置方式

```xml
<bean id="providerConfig1" class="com.mendmix.cos.CosProviderConfig">
	<property name="accessKey" value="${mendmix.cos.adapter.accessKey}" />
    <property name="secretKey" value="${mendmix.cos.adapter.secretKey}" />
	<property name="regionName" value="${mendmix.cos.adapter.regionName}" />
</bean>

<bean class="com.mendmix.cos.CosProviderServiceFacade">
    <property name="type" value="aliyun" />
	<property name="config" ref="providerConfig1" />
</bean>		
```

用法：
```java
@Autowired
private CosProviderServiceFacade cosProviderService;
//上传
cosProviderService.upload(object);
```

### Springboot 方式
```java
@Bean
@ConditionalOnProperty(value = "mendmix.cos.adapter.type")
public CosProviderServiceFacade cosProviderService(){
	CosProviderServiceFacade bean = new CosProviderServiceFacade();
	CosProviderConfig conf = new CosProviderConfig();
	String type = ResourceUtils.getAndValidateProperty("mendmix.cos.adapter.type");
	bean.setType(type);
	conf.setAccessKey(ResourceUtils.getProperty("mendmix.cos.adapter.accessKey"));
	//....其他属性
	bean.setConfig(conf);
	return bean;
}

```