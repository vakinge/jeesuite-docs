---
title: 统一认证平台
---

# 介绍
mendmix-passport是面向企业级单点登录、统一认证的一站式解决方案。支持微信、企业微信、主流开放平台OAuth、Oauth2.0，JWT、SAML2.0多种集成模式。

## 主要功能清单
   1. SSO单点登录、无域名限制
   1. 除了账号密码外还支持微信、企业微信、主流开放平台OAuth、Oauth2.0等多种登录方式
   2. 支持多个微信公众号及微信小程序模式
   3. 支持SDK,JWT、SAML2.0多种集成模式
   4. 支持单机与集群部署模式

## 特点
 - 轻量级：不依赖Shiro、Spring Secrity以及任何第三方认证框架
 - 代码简单：二开成本低，可定制性高

# 服务端部署
## 下载项目

```
git clone https://gitee.com/vakinge/mendmix-passport.git
```

## 编译及运行项目（包括前端和后端）

### 后端
```
mvn clean package -DskipTests=true
```

**最终生成部署包为**：mendmix-passport-server/target/mendmix-passport-server.jar

#### 创建数据库表

```
CREATE DATABASE IF NOT EXISTS `passport` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```
#### 数据库初始化
执行docs目录下数据库脚本: init.sql 与 testdata.sql (此为测试数据，可选)

#### 修改配置文件
修改`application-local.properties`配置内容

#### 启动
```
java -jar mendmix-passport-server.jar
# 或 （适合linux或mac 后台运行）
nohup java -jar mendmix-passport-server.jar > passport-server.out 2>&1 &
```
### 前端
切换到目录`mendmix-passport-web`,执行命令：

```bash
yarn install
yarn build:prod
```
**最终生成部署文件在目录**：mendmix-passport-web/dist下。

### 添加本地host (域名可自定义)
```
127.0.0.1 passport.mendmix.com
```
### 配置Nginx
```
server {
        listen       80;
        server_name  passport.mendmix.com;

        charset utf-8;
        location / {
          root /{这里是你的实际项目根目录}/mendmix-passport/mendmix-passport-web/dist ;
          index index.html;
        }
        location /auth {
          proxy_pass http://127.0.0.1:8080/auth;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
		location /api {
		  proxy_pass http://127.0.0.1:8080/api;
		  proxy_set_header Host $host;
		  proxy_set_header X-Real-IP $remote_addr;
		  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}
}
```
### 启动Nginx，查看效果
启动成功可以通过`passport.mendmix.com`查看。

---
# 应用集成（客户端）

## 添加依赖

```
<dependency>
  <groupId>com.mendmix</groupId>
  <artifactId>mendmix-config-client</artifactId>
 <version>[版本号]</version>
</dependency>
```
查看版本号请进入[客户端maven 仓库](https://repo1.maven.org/maven2/com/mendmix/mendmix-passport-client/)查看

## 添加配置

在项目增加如下配置：
```
mendmix.passport.baseUrl=http://passport.mendmix.com
mendmix.passport.clientId=
mendmix.passport.clientSecret=
#不需要认证的URI多个;隔开
mendmix.passport.anonymousUris=/abc/*;/status;/bar/foo
```
## 注册过滤器
```java
@Bean
public FilterRegistrationBean<SSOGlobalFilter> ssoFilterRegistration() {
	FilterRegistrationBean<SSOGlobalFilter> registration = new FilterRegistrationBean<>();
	registration.setFilter(new SSOGlobalFilter());
	registration.addUrlPatterns("/*");
	registration.setName("authFilter");
	registration.setOrder(0);
	return registration;
}
```
非springboot项目，在web.xml配置。

## 登入登出入口
 - 登录：/sso/login
 - 登出：/sso/logout
 
## 自定义session存储
默认使用本地session，不支持集群方式共享session。如果需要支持集群共享session，需要实现`SessionStorageProvider`接口，并通过以下方式注册：
```
PassportConfigHolder.setSessionStorageProvider(provider);
```
