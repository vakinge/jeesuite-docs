---
title: spring增强模块
---

# 功能说明

* spring依赖包管理，静态bean工厂，一些工具类。

# 使用说明

## 添加依赖

```
<dependency>
    <groupId>com.mendmix</groupId>
    <artifactId>mendmix-spring</artifactId>
    <version>[最新版本]</version>
</dependency>
```

## 静态bean工厂
### 初始化
>mybatis,cache,cheduler模块都默认会初始化，如果引入了就无需再初始化。
```java
public class InstanceFactoryConfiguration implements ApplicationContextAware,PriorityOrdered{

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		InstanceFactory.setInstanceProvider(new SpringInstanceProvider(applicationContext));
	}

	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}

}
```

### 使用
```
//获取bean实例
JedisProvider provider = InstanceFactory.getInstance(JedisProvider.class);
//阻塞只到context初始化
InstanceFactory.waitUtilInitialized();
```

## 全局AOP拦截器
为了确保整体框架运行上下文统一，提供了一个统一的全局拦截器用于处理框架本身的必须的一些操作,使用只需继承基类定义切面即可，代码如下：
```java
@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
public class GlobalServiceInterceptor extends mendmixSpringBaseInterceptor {

	@Override
	@Pointcut("execution(public * com.mendmix.demo.service..*.*(..))")
	public void pointcut() {}

}
```

## helper类
 -  EnvironmentHelper
 -  SpringAopHelper



