---
title: 通用模块
---

# 功能说明

common模块提供一些通用的model定义及基础工具类。通常情况`Apache Commons`和`Google Guava`提供了大量的工具类，也是`mendmix`大力推荐使用的。因此`mendmix-common`提供的工具类更多对一些工具类的二次封装或对已有工具的补充。

# 使用说明

## 添加依赖

```
<dependency>
    <groupId>com.mendmix</groupId>
    <artifactId>mendmix-common</artifactId>
    <version>[最新版本]</version>
</dependency>
```

## 工具列表
配置项 | 说明
---|---
ResourceUtils  | 本地配置加载工具，支持Properties和yaml，兼容spring.profiles.active
JsonUtils  | 基于jackson封装
DigestUtils | MD5,MD5短码
SerializeUtils | 支持Kryo、JDK、FST三种实现
HttpUtils | http请求工具,底层适配jdk原生、okhttp3、apache httpclient自动选择
BeanUtils | bean复制工具，比commons-beanutil好用一丢丢
ClassScanner | 基于包路径类扫描工具
GzipUtils | gzip加解密
DateUtils | 日期工具，继承Apache Commons日期处理函数
FormatValidateUtils | 手机号等格式校验工具
HashUtils | 一致hash生成工具
IdCardFormatVerifyHelper | 身份证格式校验及生成
NetworkUtils | telnet，ping，检查端口等
PathMatcher | 路径匹配
ReflectUtils | 反射工具
RsaSignUtils | RSA签名工具
SimpleCryptUtils | 简单加解密工具类
TokenGenerator | 生成带时效性的token
crypt包  | AES,DES,RSA,Base64，SHA1
async包   | 多线程、异步等封装工具类

## 用法举例

### http请求
```java
public static void main(String[] args) {

		HttpResponseEntity entity;
		//get
		entity = HttpUtils.get("http://www.mytest.com/query?type=1");
		System.out.println(entity);
		
		//post JSON
		String json = "{\"example\":{},\"pageNo\":1,\"pageSize\":10}";
		entity = HttpUtils.postJson("http://openapi.mytest.com/api/list", json);
		System.out.println(entity);
		
		//basicAuth
		HttpRequestEntity requestEntity = HttpRequestEntity.create(HttpMethod.GET).basicAuth("admin", "123456");
		entity = HttpUtils.execute("http://mytest.com/login", requestEntity);
		System.out.println(entity);
		//自定义header，查询参数
		HttpRequestEntity.create(HttpMethod.POST)
		                 .body(json) //
		                 .header("x-request-id", UUID.randomUUID().toString()) //header
		                 .queryParam("type", "1"); 
		
		entity = HttpUtils.execute("http://mytest.com/add", requestEntity);
		System.out.println(entity);
		//上传文件
		HttpRequestEntity.create(HttpMethod.POST).fileParam("file", new File("/home/test.zip"));
		entity = HttpUtils.execute("http://mytest.com/upload", requestEntity);
		System.out.println(entity);
	}
```

### 获取配置文件
```java
public static void main(String[] args) {
    	//包含配置
    	boolean containsProperty = ResourceUtils.containsProperty("key1");
    	
    	ResourceUtils.getProperty("key1");
    	ResourceUtils.getProperty("key1", "默认值");
    	//指定key任意配置
    	String value = ResourceUtils.getAnyProperty("key1","key2","key3");
    	//获取所有配置（拷贝）
    	Properties properties = ResourceUtils.getAllProperties();
    	//按前缀获取所有配置（拷贝）
    	properties = ResourceUtils.getAllProperties("prefix.");
}
```

### 重试异步任务
```java
public static void main(String[] args) {
	    //默认执行器
		RetryAsyncTaskExecutor.execute(new RetryTask() {
			
			@Override
			public String traceId() {
				//用于日志输出前缀，方便标识
				return "orderNo:123456";
			}
			
			@Override
			public boolean process() throws Exception {
				//异步处理逻辑
				return true;
			}
		});
		//自定义重试器
		RetryAsyncTaskExecutor executor = new RetryAsyncTaskExecutor("name1", 10, 2000, 1);
	}
```
### 重试代码块
```java
new SimpleRetry<String>(new ICaller<String>() {
    @Override
	public String call(Object... args) {
		//业务代码
		return null;
	}
}).doRetry(3);
```
