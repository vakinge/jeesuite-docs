module.exports = {
	theme: "antdocs",
	title: "Mendmix - 100%开源分布式云原生架构一站式解决方案",
	description: "100%开源分布式云原生架构一站式解决方案",
	base: "/",
	head: [
		["link", {
			rel: "icon",
			href: "/assets/logo.png"
		}]
	],
	markdown: {
		lineNumbers: false,
	},
	themeConfig: {
		smoothScroll: true,
		nav: require("./config/nav"),
		sidebar: require("./config/sidebar"),
		lastUpdated: "Last Updated",
		repo: "https://github.com/vakinge",
		editLinks: false,
		backToTop: true,
		// ads: {
		// 	style: 2,
		// 	speed: 2000,
		// 	items: [{
		// 			text: 'Ads details here',
		// 			image: 'https://images.gitee.com/uploads/images/2020/0806/200339_a26c1597_12388.jpeg',
		// 			link: 'https://gitee.com/vakinge'
		// 		}
		// 	]
		// },
	},
};
