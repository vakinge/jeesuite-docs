module.exports = [
  {
    text: "首页",
    link: "/"
  },
  {
    text: "指南",
    link: "/guide/"
  },
  {
    text: "文档",
    link: "/docments/"
  },
  {
    text: "教程",
    link: "/course/"
  }
];
