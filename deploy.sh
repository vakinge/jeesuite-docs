#!/usr/bin/env sh

# abort on errors
set -e

export NODE_OPTIONS=--openssl-legacy-provider
# build
npm run build

# navigate into the build output directory
rm -rf ../jeesuite-libs/docs/* && cp -r docs/.vuepress/dist/* ../jeesuite-libs/docs/

# if you are deploying to a custom domain
# echo 'www.example.com' > CNAME

# if you are deploying to https://<USERNAME>.github.io
# git push -f git@github.com:<USERNAME>/<USERNAME>.github.io.git master

# if you are deploying to https://<USERNAME>.github.io/<REPO>
# git push -f git@github.com:<USERNAME>/<REPO>.git master:gh-pages
